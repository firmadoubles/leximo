import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import ScrollToTop from 'react-router-scroll-top'
import CookieBar from './components/CookieBar/CookieBar';
import Navbar from './components/Navbar/Navbar';
import SubpageNavbar from './components/SubpageNavbar/SubpageNavbar';
import Intro from './components/Intro/Intro';
import Team from './components/Team/Team';
import Persons from './components/Persons/Persons';
import Numbers from './components/Numbers/Numbers';
import OfferIntro from './components/Offer/OfferIntro';
import OfferBox from './components/Offer/OfferBox';
import Diagram from './components/Offer/Diagram';
import Reference from './components/Reference/Reference';
import Valuation from './components/Valuation/Valuation';
import Contact from './components/Contact/Contact';
import PrivacyPolicy from './components/PrivacyPolicy/PrivacyPolicy';
import Cookies from './components/Cookies/Cookies';
import Footer from './components/Footer/Footer';
import tlumaczeniaUwierzytelnioneImage from './assets/tlumaczenia-uwierzytelnione.png';
import tlumaczeniaZwykleImage from './assets/tlumaczenia-zwykle.png';
import tlumaczeniaUstneImage from './assets/tlumaczenia-ustne.png';
import './App.css';

class HomeView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false
    };
  }

  componentDidMount() {
    fetch("https://api.leximo.pl/wp-json/wp/v2/pages/34")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            uwierzytelnione: result.content.rendered
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
      
    fetch("https://api.leximo.pl/wp-json/wp/v2/pages/40")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            zwykle: result.content.rendered
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
      
    fetch("https://api.leximo.pl/wp-json/wp/v2/pages/42")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            ustne: result.content.rendered
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
      
    fetch("https://api.leximo.pl/wp-json/wp/v2/pages/44")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            diagram: result.content.rendered
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  render() {
    return (
      <>
        <CookieBar />
        <Navbar />
        <Intro />
        <div id="teamPage">
          <Team />
          <Persons />
          <Numbers />
        </div>
        <div id="offerPage">
          <OfferIntro />
          <OfferBox
            title="TŁUMACZENIA UWIERZYTELNIONE"
            img={tlumaczeniaUwierzytelnioneImage}
            txt={ this.state.uwierzytelnione }>
          </OfferBox>
          <OfferBox
            title="TŁUMACZENIA ZWYKŁE"
            img={tlumaczeniaZwykleImage}
            txt={this.state.zwykle}
            ></OfferBox>
          <OfferBox
            title="TŁUMACZENIA USTNE"
            img={tlumaczeniaUstneImage}
            txt={ this.state.ustne }>
          </OfferBox>
          <Diagram txt={ this.state.diagram }>
          </Diagram>
        </div>
        <div id="referencePage">
          <Reference />
        </div>
        <div id="valuationPage">
          <Valuation />
        </div>
        <div id="contactPage">
          <Contact />
        </div>
        <Footer />
      </>
    );
  }
};

const PolicyView = () => {
  return (
    <>
      <CookieBar />
      <SubpageNavbar />
      <Intro />
      <PrivacyPolicy />
      <Footer />
    </>
  );
};

const CookiesView = () => {
  return (
    <>
      <CookieBar />
      <SubpageNavbar />
      <Intro />
      <Cookies />
      <Footer />
    </>
  );
};

const App = () => (
  <BrowserRouter>    
    <ScrollToTop>
      <Switch>
        <Route exact path="/" component={HomeView} />
        <Route path="/polityka-prywatnosci" component={PolicyView} />
        <Route path="/cookies" component={CookiesView} />
      </Switch>
    </ScrollToTop>
  </BrowserRouter>
)

export default App;
