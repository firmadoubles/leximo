import React from 'react';
import Number from './Number';
import styles from './Numbers.module.scss';

const Numbers = (props) => (
  <div className={styles.wrapper}>
    <Number
      number="10"
      text="lat doświadczenia"
    />
    <Number
      number="50000"
      text="przetłumaczonych stron"
    />
    <Number
      number="150"
      text="zadowolonych klientów"
    />
  </div>
);

export default Numbers;