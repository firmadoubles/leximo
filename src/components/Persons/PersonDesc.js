import React from 'react';
// import PersonTitle from './PersonTitle';
// import PersonDesc from './PersonDesc';
// import PersonImage from './PersonImage';
// import panasImage from '../../assets/anna-panas-galloway.png';
// import mazurImage from '../../assets/marcin-mazur.png';
import styles from './PersonDesc.module.scss';

const PersonDesc = (props) => (
  <div className={styles.wrapper} dangerouslySetInnerHTML={{__html: props.text}}></div>
);

export default PersonDesc;