import React, { Component } from 'react';
import PersonTitle from './PersonTitle';
import PersonDesc from './PersonDesc';
import PersonImage from './PersonImage';
import panasImage from '../../assets/anna-panas-galloway.png';
import mazurImage from '../../assets/marcin-mazur.png';
import styles from './Persons.module.scss';

class Persons extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }

  componentDidMount() {
    fetch("https://api.leximo.pl/wp-json/wp/v2/pages/22")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            anna: result.content.rendered
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );

    fetch("https://api.leximo.pl/wp-json/wp/v2/pages/24")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            marcin: result.content.rendered
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  render() {
    return (
      <div className={styles.personsContainer}>
        <div className={styles.wrapper} data-aos="fade-left">
          <PersonTitle
            desc="TŁUMACZ PRZYSIĘGŁY JĘZYKA ANGIELSKIEGO"
          >Anna<br />Panas-Galloway</PersonTitle>
          <PersonImage
            img={panasImage}
          />
          <PersonDesc
            text={this.state.anna}
          />
        </div>
        <div className={styles.wrapper} style={{'borderBottom': '1px solid #d1dadc'}} data-aos="fade-right">
          <PersonTitle
            desc="TŁUMACZ PRZYSIĘGŁY JĘZYKA ANGIELSKIEGO"
          >Marcin Mazur</PersonTitle>
          <PersonImage
            img={mazurImage}
          />
          <PersonDesc
            text={this.state.marcin}
          />
        </div>
      </div>
    )
  }
};

export default Persons;