import React from 'react';
import styles from './Diagram.module.scss';

const Diagram = (props) => (
  <div className={styles.wrapper}>
    <aside
      className={styles.diagram}
      data-aos="zoom-in"
    >
      <h2>Nasze<br/ >specjalizacje</h2>
      <p className={styles.tl}>Prawo</p>
      <p className={styles.tr}>Finanse</p>
      <p className={styles.br}>Biznes</p>
    </aside>
    <article data-aos="fade-left" dangerouslySetInnerHTML={{__html: props.txt}}></article>
  </div>
);

export default Diagram;