import React from 'react';
import Titles from '../Titles/Titles';
import styles from './OfferIntro.module.scss';

const OfferIntro = (props) => (
  <div
    className={styles.wrapper}
    data-aos="fade-down"
  >
    <Titles
      title="Oferta"
      subtitle="Co możemy zaoferować?"
    />
    <p>Oferujemy profesjonalne tłumaczenia pisemne i ustne, zarówno w formie uwierzytelnionej, jak i zwykłej. Pracujemy przede wszystkim z językiem angielskim, ale nie oznacza to, że inne języki są nam obce. Nasze usługi obejmują:</p>
  </div>
); 

export default OfferIntro;