import React from 'react';
// import Titles from '../Titles/Titles';
import styles from './OfferBox.module.scss';

const OfferBox = (props) => (
  <div className={styles.wrapper}>
    <aside data-aos="fade-right">
      <h3 style={{backgroundImage : `url(${props.img})`}}>{props.title}</h3>
    </aside>
    <article data-aos="fade-left" dangerouslySetInnerHTML={{__html: props.txt}}></article>
  </div>
);

export default OfferBox;