import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from './CookieBar.module.scss';

function remove() {
  let el = document.getElementById('cookie-bar');
  el.remove();
}

const CookieBar = () => (
  <div className={styles.wrapper} id="cookie-bar">
    <p>Strona na której się znajdujesz korzysta z plików cookie. Korzystając ze strony wyrażasz zgodę na ich używanie. <NavLink className={styles.link} to="/cookies">Informacja o plikach cookie.</NavLink></p>
    <div className={styles.hide}>
      <button className={styles.hideBtn} onClick={remove}>X</button>
    </div>
  </div>
);

export default CookieBar;