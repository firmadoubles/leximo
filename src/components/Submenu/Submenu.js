import React, { Component } from 'react';
import styles from './Submenu.module.scss';
// import './Menu.css';

class Submenu extends Component {
    render() {
        return (
          <>
            <ul className={styles.wrapper}>
              <li>
                <a href="/#team">
                    Zespół
                  </a>
              </li>
              <li>
                <a href="/#offerPage">
                  Oferta
                </a>
              </li>
              <li>
                <a href="/#referencePage">
                  Referencje
                </a>
              </li>
              <li>
                <a href="/#valuationPage">
                  Wycena
                </a>
              </li>
              {/* <li>
                <a href="/polityka-prywatnosci" className={styles.active}>
                  Polityka prywatności
                </a>
              </li> */}
              <li>
                <a href="/#contactPage">
                  Kontakt
                </a>
              </li>
            </ul>
            <ul className={styles.language}>
              <li>
                <a href="http://en.leximo.pl/">
                  EN
                </a>
              </li>
            </ul>
          </>
        )
    }
};

export default Submenu;