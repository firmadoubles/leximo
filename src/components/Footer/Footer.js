import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from './Footer.module.scss';

const Footer = () => (
  <div className={styles.wrapper}>
    <div className={styles.container}>
      <p className={styles.text}>Leximo Mazur Panas-Galloway Tłumacze Przysięgli Spółka Partnerska | ul. Kminkowa 47A lok. 4, 62-064 Plewiska, Polska | NIP: 7773358581 | REGON: 385525750 | KRS: 0000827758 | Sąd Rejonowy Poznań – Nowe Miasto i Wilda w Poznaniu, VIII Wydział Gospodarczy Krajowego Rejestru Sądowego<br /><NavLink className={styles.link} to="/polityka-prywatnosci">POLITYKA PRYWATNOŚCI</NavLink></p>
    </div>
  </div>
);

export default Footer;