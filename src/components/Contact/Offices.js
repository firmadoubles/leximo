import React from 'react';
import { Tabs, TabLink, TabContent} from 'react-tabs-redux';
import styles from './Offices.module.scss';

const Offices = () => (
  <div className={styles.wrapper}>
    <Tabs className={styles.content}>
      <ul className={styles.officeNavi}>
        <li>
          {/* <button type="button" className={styles.officeNaviBtn}> */}
            <TabLink
              className={styles.officeNaviBtn}
              activeClassName={styles.officeNaviBtnActive}
              to="tab1"
            >Bydgoszcz</TabLink>
        </li>
        <li>
          <TabLink
            className={styles.officeNaviBtn}
            activeClassName={styles.officeNaviBtnActive}
            to="tab2"
          >Poznań</TabLink>
        </li>
        <li>
          <TabLink
            className={styles.officeNaviBtn}
            activeClassName={styles.officeNaviBtnActive}
            to="tab3"
          >Warszawa</TabLink>
        </li>
      </ul>
      <div>
        <TabContent for="tab1">
          <div
            className={styles.officeDesc}
          >
            <h4>Biuro w Bydgoszczy</h4>
            <p>ul. 16 Pułku Ułanów Wlkp. 1/7<br />
            85-319 Bydgoszcz<br />
            email: <b>biuro@leximo.pl</b><br />
            tel. <b>+48 605 833 663</b></p>
          </div>
        </TabContent>
        <TabContent for="tab2">
          <div className={styles.officeDesc}>
            <h4>Biuro w Poznaniu</h4>
            <p>ul. Sarmacka 73<br />
            61-616 Poznań<br />
            email: <b>biuro@leximo.pl</b><br />
            tel. <b>+48 605 833 663</b></p>
          </div>
        </TabContent>
        <TabContent for="tab3">
          <div className={styles.officeDesc}>
            <h4>Biuro w Warszawie</h4>
            <p>ul. Maszewska 20/50<br />
            01-925 Warszawa<br />
            email: <b>biuro@leximo.pl</b><br />
            tel. <b>+48 531 971 555</b></p>
          </div>
        </TabContent>
      </div>
    </Tabs>
  </div>
);

export default Offices;