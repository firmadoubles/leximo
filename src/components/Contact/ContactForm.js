import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import axios from 'axios';
import styles from './ContactForm.module.scss';
const API_PATH = '/api/mailer.php';

class ContactForm extends Component {

  state = {
    fileName: 'DODAJ PLIK',
    fname: '',
    ftel: '',
    fmail: '',
    fin: '',
    fout: '',
    ftype: '',
    fmessage: '',
    file: '',
    mailSent: false,
    error: null,
  };

  handleChange()
  {
    var e=document.getElementById("file");
    console.log(e.files[0]);
    this.setState({
      fileName: e.files[0].name,
      file: e.files[0],
    });
  }

  handleFormSubmit = (e) => {
    e.preventDefault();
    let formData = new FormData();
    var ae=document.getElementById("file");
    formData.append("fname", this.state.fname);
    formData.append("fmail", this.state.fmail);
    formData.append("ftel", this.state.ftel);
    formData.append("fin", this.state.fin);
    formData.append("fout", this.state.fout);
    formData.append("ftype", this.state.ftype);
    formData.append("fmessage", this.state.fmessage);
    formData.append("fileName", this.state.fileName);
    formData.append("file", ae.files[0]);
    axios({
      method: 'post',
      url: `${API_PATH}`,
      headers: { 'content-type': 'multipart/form-data' },
      data: formData
    })
      .then(result => {
        this.setState({
          mailSent: result.data.sent,
          fname: '',
          ftel: '',
          fmail: '',
          fin: '',
          fout: '',
          ftype: '',
          fmessage: '',
          file: '',
          fileName: 'DODAJ PLIK'
        })
      })
      .catch(error => this.setState({ error: error.message }));
  };

  render () {
    return (
      <div className={styles.wrapper}>
        <div className={styles.content}>
        {this.state.mailSent ?
          <p>Wiadomość została wysłana.</p>
          :
          <form onSubmit={this.handleFormSubmit}>
            <div className={styles.col1}>
              <input type="text" placeholder="IMIĘ I NAZWISKO | FIRMA"
                name="fname"
                value={this.state.fname}
                onChange={e => this.setState({ fname: e.target.value })}
              />
            </div>
            <div className={styles.col2}>
              <div>
                <input
                  type="text"
                  placeholder="NUMER TELEFONU"
                  name="ftel"
                  value={this.state.ftel}
                  onChange={e => this.setState({ ftel: e.target.value })}
                />
              </div>
              <div>
                {/* <label className={styles.movingLabel}></label> */}
                <input type="text" placeholder="EMAIL"
                  name="fmail"
                  value={this.state.fmail}
                  onChange={e => this.setState({ fmail: e.target.value })}
                />
              </div>
            </div>
            <div className={styles.col2}>
              <div>
                {/* <label className={styles.movingLabel}></label> */}
                <input type="text" placeholder="JĘZYK ŹRÓDŁOWY"
                  name="fin"
                  value={this.state.fin}
                  onChange={e => this.setState({ fin: e.target.value })}
                />
              </div>
              <div>
                {/* <label className={styles.movingLabel}></label> */}
                <input type="text" placeholder="JĘZYK DOCELOWY"
                  name="fout"
                  value={this.state.fout}
                  onChange={e => this.setState({ fout: e.target.value })}
                />
              </div>
            </div>
            <div className={styles.col1} style={{'margin': 0}}>
              <label>
                <input type="radio" name="type"
                  value="Tłumaczenie przysięgłe"
                  onChange={e => this.setState({ ftype: e.target.value })}
                />
                Tłumaczenie przysięgłe
              </label>
            </div>
            <div className={styles.col1} style={{'margin': 0}}>
              <label>
                <input type="radio" name="type"
                  value="Tłumaczenie zwykłe"
                  onChange={e => this.setState({ ftype: e.target.value })}
                />
                Tłumaczenie zwykłe
              </label>
            </div>
            <div className={styles.col1}>
              <label>
                <input type="radio" name="type"
                  value="Korekta"
                  onChange={e => this.setState({ ftype: e.target.value })}
                />
                Korekta
              </label>
            </div>
            <div className={styles.col1}>
              <div>
                {/* <label className={styles.movingLabel}></label> */}
                <textarea type="text" placeholder="Treść wiadomości"
                  name="fmessage"
                  value={this.state.fmessage}
                  onChange={e => this.setState({ fmessage: e.target.value })}
                ></textarea>
              </div>
            </div>
            <div className={styles.col1}>
              <input type="file" name="type" id="file" className={styles.inputFile} onChange={ (e) => this.handleChange(e.target.files) } />
              <label htmlFor="file">{ this.state.fileName }</label>
            </div>
            <p className={styles.info}>Wypełnienie formularza i przekazanie swoich danych Leximo Mazur Panas-Galloway Tłumacze Przysięgli Spółka Partnerska jest równoznaczne z wyrażeniem zgody na ich gromadzenie i przetwarzanie w sposób zgodny z <NavLink className={styles.link} to="/polityka-prywatnosci">polityką prywatności</NavLink>.</p>
            <button type="submit" className={styles.formSubmit}>Wyślij</button>
          </form>
        }
        </div>
      </div>
    )
  }
};

export default ContactForm;