import React from 'react';
import Titles from '../Titles/Titles';
import Offices from './Offices';
import ContactForm from './ContactForm';
import styles from './Contact.module.scss';

const Contact = (props) => (
  <>
    <Titles
      title="Kontakt"
      subtitle="Pokonaj z nami bariery językowe..."
    />
    <div className={styles.wrapper}>
      <Offices />
      <ContactForm />
    </div>
  </>
);

export default Contact;