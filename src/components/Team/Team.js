import React, { Component } from 'react';
import Titles from '../Titles/Titles';
import styles from './Team.module.scss';

class Team extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false
    };
  }

  componentDidMount() {
    fetch("https://api.leximo.pl/wp-json/wp/v2/pages/13")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            pageTitle: result.title.rendered,
            content: result.content.rendered
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  render() {
    return (
      <div
        className={styles.container}
      >
        <div
          data-aos="fade-right"
          className={styles.wrapper}
        >
          <Titles
            title="Zespół"
            subtitle="Poznaj nas"
          />
          <div className={styles.text} dangerouslySetInnerHTML={{__html: this.state.content}}>
          </div>
        </div>
        <div className={styles.bgImage}></div>
      </div>
    )
  }
};

export default Team;