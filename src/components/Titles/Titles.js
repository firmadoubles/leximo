import React from 'react';
import styles from './Titles.module.scss';

const Titles = (props) => (
  <div className={styles.wrapper}>
    <h2>{props.title}</h2>
    <h3>{props.subtitle}</h3>
  </div>
);

export default Titles;