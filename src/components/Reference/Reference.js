import React, { Component } from 'react';
import styles from './Reference.module.scss';
import Titles from '../Titles/Titles';
import ItemsCarousel from 'react-items-carousel';
import './Reference.css';

const noOfItems = 4;
const noOfCards = 1;
const autoPlayDelay = 15000;
const chevronWidth = 80;

class Reference extends Component {
  
  state = {
    activeItemIndex: 0,
    // content
  };

  componentDidMount() {
    this.interval = setInterval(this.tick, autoPlayDelay);
    fetch("https://api.leximo.pl/wp-json/wp/v2/posts?categories=2")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            content: result
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  tick = () => this.setState(prevState => ({
    activeItemIndex: (prevState.activeItemIndex + 1) % (noOfItems-noOfCards + 1),
  }));

  onChange = value => this.setState({ activeItemIndex: value });

  render () {
    return (
      <div className={styles.wrapper}>
        <aside className={styles.titleBox}>
          <Titles
            title="Referencje"
            subtitle="O nas, bez nas..."
          />
        </aside>
        <article className={styles.textBox}>
          <div className={styles.box}>
            {this.state.content === '' ?
              <p>Ładowanie</p>
              :
              <ItemsCarousel
                gutter={12}
                numberOfCards={noOfCards}
                activeItemIndex={this.state.activeItemIndex}
                requestToChangeActive={this.onChange}
                rightChevron={<button className="sliderArrowRight">{'>'}</button>}
                leftChevron={<button className="sliderArrowLeft">{'<'}</button>}
                chevronWidth={chevronWidth}
                outsideChevron
              >
                {this.state.content && this.state.content.map((post, index) => (
                  <div className={styles.slideBox} key={index}>
                    <div dangerouslySetInnerHTML={{__html: post.excerpt.rendered.replace(/[\xc2\xa0]+/g, ' ') }}></div>
                    <p className={styles.signature}>{ post.title.rendered }</p>
                  </div>
                ))}
              </ItemsCarousel>
            }
          </div>
        </article>
        <span className={styles.pen}></span>
        <span className={styles.ink}></span>
      </div>
    )
  }
};

export default Reference;