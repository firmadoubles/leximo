import React, { Component } from 'react';
import introImg from '../../assets/intro-bg.png';
import styles from './Intro.module.scss';

class Intro extends Component {
    render() {
        return (
          <div data-aos="fade-left" className={styles.wrapper}>
            <img src={introImg} alt="Dajemy Ci słowo" className={styles.image} />
            <p className={styles.sloganBig}>Dajemy Ci słowo...</p>
          </div>
        )
    }
};

export default Intro;