import React from 'react';
import Titles from '../Titles/Titles';
import { Link } from "react-scroll";
import styles from './Valuation.module.scss';

const Valuation = (props) => (
  <div className={styles.wrapper}>
    <Titles
      title="Wycena"
      subtitle="Czyli co i za ile?"
    />
    <div className={styles.textBox}>
      <p>Każdy dokument wyceniamy indywidualnie.</p>
      <p>Aby otrzymać bezpłatną wycenę oraz poznać termin i koszt tłumaczenia, prześlij dokument na adres <Link
          className={styles.link}
          to="contactPage"
          smooth={true}
          duration={600}
          offset={-100}
      >biuro@leximo.pl</Link> lub <Link
          className={styles.link}
          to="contactPage"
          smooth={true}
          duration={600}
          offset={-100}
      >wypełnij formularz kontaktowy</Link>.</p>
      <p>Oferujemy tłumaczenia w trzech trybach: zwykłym, przyspieszonym i ekspresowym.</p>
    </div>
  </div>
);

export default Valuation;