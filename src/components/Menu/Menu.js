import React, { Component } from 'react';
import { Link } from "react-scroll";
import { isMobile } from 'react-device-detect';
import styles from './Menu.module.scss';

  class Menu extends Component {
  hideMenu = () => {
    if (isMobile) {
      let link = document.getElementById('menuBtn');
      link.click();
    }
  };

  render() {
    return (
      <>
        <ul className={styles.wrapper}>
          <li>
            <Link
              activeClass={styles.active}
              to="teamPage"
              spy={true}
              smooth={true}
              duration={600}
              offset={-100}
              onClick={this.hideMenu}
            >
              Zespół
            </Link>
          </li>
          <li>
            <Link
              activeClass={styles.active}
              to="offerPage"
              spy={true}
              smooth={true}
              duration={600}
              offset={-100}
              onClick={this.hideMenu}
            >
              Oferta
            </Link>
          </li>
          <li>
            <Link
                activeClass={styles.active}
                to="referencePage"
                spy={true}
                smooth={true}
                duration={600}
                offset={-100}
                onClick={this.hideMenu}
            >
              Referencje
            </Link>
          </li>
          <li>
            <Link
                activeClass={styles.active}
                to="valuationPage"
                spy={true}
                smooth={true}
                duration={600}
                offset={-100}
                onClick={this.hideMenu}
            >
              Wycena
            </Link>
          </li>
          {/* <li>
            <NavLink
              activeClassName={styles.navItemLinkActive}
              className={styles.navItemLink}
              to="/polityka-prywatnosci">
              Polityka prywatności
            </NavLink>
          </li> */}
          <li>
            <Link
              activeClass={styles.active}
              to="contactPage"
              spy={true}
              smooth={true}
              duration={600}
              offset={-100}
              onClick={this.hideMenu}
            >
              Kontakt
            </Link>
          </li>
        </ul>
        <ul className={styles.language}>
          <li>
            <a href="http://en.leximo.pl/">
              EN
            </a>
          </li>
        </ul>
      </>
    )
  }
};

export default Menu;