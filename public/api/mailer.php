<?php
  header("Access-Control-Allow-Origin: *");
  // var_dump($_POST);
  // var_dump($_FILES);
  // die();

  // if (empty($_POST['fname']) && empty($_POST['fmail'])) die();


  if ($_POST)
    {
    
    if ($_FILES)  {
      $uploads_dir = '../uploads';
      $tmp_name = $_FILES["file"]["tmp_name"];
      // basename() may prevent filesystem traversal attacks;
      // further validation/sanitation of the filename may be appropriate
      $name = basename($_FILES["file"]["name"]);
      move_uploaded_file($tmp_name, "$uploads_dir/$name");
    }

    // set response code - 200 OK

    http_response_code(200);
    $subject = $_POST['fname'];
    $to = "biuro@leximo.pl";
    $from = $_POST['fmail'];

    // data

    $msg = "Imię i nazwisko: " . $_POST['fname'] . "<br />Numer telefonu: " . $_POST['ftel'] . "<br />Język źródłowy: " . $_POST['fin'] . "<br />Język docelowy: " . $_POST['fout'] . "<br />Typ tłumaczenia: " . $_POST['ftype'] . "<br />Treść wiadomości: " . $_POST['fmessage'] . "<br />Załączony plik: ";
    $msg .= '<a href="https://www.leximo.pl/uploads/' . $name . '" target="_blank">' . $name . '</a>';
    // $msg = "Imię i nazwisko: " . $_POST['fname'];

    // Headers

    // $headers = "MIME-Version: 1.0\r\n";
    // $headers .= "Content-type:text/html;charset=UTF-8\r\n";
    // $headers .= "From: biuro@leximo.pl";
    // $headers = 'From: biuro@leximo.pl\r\n';
    // $headers .= 'Reply-to:' . $_POST['fmail'] . "\r\n" ;
    // $headers .= 'X-Mailer: PHP/' . phpversion();

    $headers = array(
      'MIME-Version' => '1.0',
      'Content-type' => 'text/html;charset=UTF-8',
      'From' => "biuro@leximo.pl",
      'Reply-To' => $_POST['fmail'],
      'X-Mailer' => 'PHP/' . phpversion()
    );

    // var_dump($to);
    // var_dump($subject);
    // var_dump($msg);
    // var_dump($headers);
    // die();
    mail($to, $subject, $msg, $headers);

    // echo json_encode( $_POST );

    echo json_encode(array(
      "sent" => true
    ));
    }
    else
    {

    // tell the user about error

    echo json_encode(["sent" => false, "message" => "Something went wrong"]);
    }

?>